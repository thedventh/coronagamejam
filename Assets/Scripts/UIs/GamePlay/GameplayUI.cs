﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;

namespace UI
{
    public class GameplayUI : MonoBehaviour
    {
        [SerializeField] Animator playerDamagedPanelAnimator;
        [SerializeField] Animator HPDamagedAnimator;
        [SerializeField] Text healthText;

        [SerializeField] Text enemySpawnedNumberText;
        [SerializeField] Text enemyKilledNumberText;

        IEnumerator shakeCamCoroutine;
        GameplayManager manager;
        int enemyKilled;

        private void OnEnable()
        {
            UIEvents.OnPlayerDamagedE += OnPlayerDamaged;
            UIEvents.OnPlayerHPChangeE += UpdatehealthPoint;
            UIEvents.OnEnemySpawnedE += UpdateEnemyNumber;
            GlobalGameplayEvents.OnEnemyDeadE += UpdateEnemyKilled;
        }

        private void OnDisable()
        {
            UIEvents.OnPlayerDamagedE -= OnPlayerDamaged;
            UIEvents.OnPlayerHPChangeE -= UpdatehealthPoint;
            UIEvents.OnEnemySpawnedE -= UpdateEnemyNumber;
            GlobalGameplayEvents.OnEnemyDeadE -= UpdateEnemyKilled;
        }

        private void Awake()
        {
            manager = GameObject.Find("GameplayManager").GetComponent<GameplayManager>();
            enemyKilled = 0;            
        }

        void OnPlayerDamaged()
        {
            ShakeCamera();
        }

        void UpdatehealthPoint(int _currenHealth)
		{
            healthText.text = _currenHealth.ToString();
		}

        void UpdateEnemyNumber(int _curren, int _max)
        {
            enemySpawnedNumberText.text = _curren + "/" + _max;
        }

        void UpdateEnemyKilled()
        {
            enemyKilled++;
            enemyKilledNumberText.text = enemyKilled.ToString();
        }

        public void ShakeCamera()
        {
            if (shakeCamCoroutine != null)
                StopCoroutine(shakeCamCoroutine);

            shakeCamCoroutine = ShakeCameraCoroutine(0.5f);
            StartCoroutine(shakeCamCoroutine);
        }

        Vector3 shakeTargetLocationContainer;
        IEnumerator ShakeCameraCoroutine(float _duration)
        {
            playerDamagedPanelAnimator.SetTrigger("damaged");
            HPDamagedAnimator.SetTrigger("damaged");
            while (_duration > 0)
            {
                shakeTargetLocationContainer = new Vector3(UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f), manager.GetMainCam().transform.position.z);

                manager.GetMainCam().transform.position = Vector3.MoveTowards(manager.GetMainCam().transform.position, shakeTargetLocationContainer, 10 * Time.deltaTime);

                _duration -= Time.deltaTime;

                yield return new WaitForEndOfFrame();
            }

            playerDamagedPanelAnimator.SetTrigger("idle");

            playerDamagedPanelAnimator.ResetTrigger("damaged");
            HPDamagedAnimator.ResetTrigger("damaged");

            manager.GetMainCam().transform.position = new Vector3(0, 0, manager.GetMainCam().transform.position.z);

            UIEvents.OnAfterPlayerDamaged();
        }
    }
}
