﻿using System.Collections.Generic;
using UnityEngine;

public class RandomEnemySpawner : MonoBehaviour
{
    public List<GameObject> toSpawnList;
    public float spawnRate = 3;
    public Transform spawnPoint;

    float timerOnRun;

    void Update()
    {
        if(timerOnRun <= 0)
        {
            timerOnRun = spawnRate;
            Instantiate(toSpawnList[Random.Range(0, toSpawnList.Count)], new Vector3(Random.Range(-8, 8), spawnPoint.transform.position.y, 0), spawnPoint.transform.rotation);
        }

        timerOnRun -= Time.deltaTime;
    }
}
