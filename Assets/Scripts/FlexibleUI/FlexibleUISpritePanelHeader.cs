﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class FlexibleUISpritePanelHeader : FlexibleUI
{
    private Image image;

    void Awake()
    {
        image = GetComponent<Image>();
        base.Initialize();
    }

    protected override void OnSkinUI()
    {
        base.OnSkinUI();
        image.sprite = flexibleUIData.spritePanelHeader;
    }
}
