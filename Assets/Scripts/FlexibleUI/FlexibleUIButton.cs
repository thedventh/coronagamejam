﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ButtonType
{
    basic,
    alternate,
    exit
}

[RequireComponent (typeof(Button))]
public class FlexibleUIButton : FlexibleUI

{
    private Button button;
    public ButtonType buttonType;

    void Awake()
    {
        button = GetComponent<Button>();
        base.Initialize();
    }

    protected override void OnSkinUI()
    {
        base.OnSkinUI();
        button.colors = flexibleUIData.buttonColorBlock;

        switch(buttonType)
        {
            case ButtonType.basic:
                button.image.sprite = flexibleUIData.spriteButtonBasic;
            break;
            case ButtonType.alternate:
                button.image.sprite = flexibleUIData.spriteButtonAlternate;
            break;
            case ButtonType.exit:
                button.image.sprite = flexibleUIData.spriteButtonExit;
            break;
        }
    }

    
	
}
