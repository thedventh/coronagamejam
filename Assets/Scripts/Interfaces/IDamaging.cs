﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamaging
{
    void Init(int _targetFaction);
}
