﻿using UnityEngine;

public interface IRotationBehaviour
{
    void Rotate(Vector3 _target);
}
