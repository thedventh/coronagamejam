﻿using UnityEngine;

public interface IMoveBehaviour
{
    void SetSpeedMultiplier(float _multiplier);
    void Move(Vector3 _targetPos);
}
