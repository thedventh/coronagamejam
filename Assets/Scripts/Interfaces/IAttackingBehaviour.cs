﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAttackingBehaviour
{
    void Fire();
}
