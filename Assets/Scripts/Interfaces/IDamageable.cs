﻿
public interface IDamageable
{
    void TakeDamage(int _damage);
    void Die();
    event System.Action OnDamagedE;
    event System.Action OnAfterDamagedE;

    event System.Action OnDiedE;
}
