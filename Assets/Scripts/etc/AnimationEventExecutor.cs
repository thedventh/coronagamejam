﻿using UnityEngine;

public class AnimationEventExecutor : MonoBehaviour
{
    public System.Action AnimationEvent;

    public void ExecuteAnimationEvent()
    {
        AnimationEvent?.Invoke();
    }
}
