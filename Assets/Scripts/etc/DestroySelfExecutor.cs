﻿using UnityEngine;

public class DestroySelfExecutor : MonoBehaviour
{
    public void DestroySelf()
    {
        Destroy(gameObject);
    }
}
