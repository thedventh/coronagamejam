﻿
public static class PublicParams 
{
    public static readonly string playerTag = "Player";
    public static readonly string enemyTag = "enemy";
    public static readonly string bulletTag = "bullet";
    public static readonly string playerBulletTag = "PlayerBullet";
    public static readonly string enemyBulletTag = "EnemyBullet";

    public static readonly float minimalScale = 0.5f;

    public static bool IsEnemyFaction(int _faction)
    {
        return _faction < 0;
    }

    public static bool IsPlayerFaction(int _faction)
    {
        return _faction > 0;
    }

}

public enum Factions
{
    neutral = 0,
    player = 1,
    enemy = -1
}
