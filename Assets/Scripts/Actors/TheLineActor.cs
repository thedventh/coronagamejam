﻿using UnityEngine;

namespace Actors
{
    public class TheLineActor : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if(collision.gameObject != null)
            {
                if(collision.gameObject.tag == PublicParams.enemyTag)
                {
                    collision.gameObject.GetComponent<IEnemyActor>().Die();
                    GlobalGameplayEvents.OnEnemyCrossTHeLine();
                }
            }
        }
    }
}