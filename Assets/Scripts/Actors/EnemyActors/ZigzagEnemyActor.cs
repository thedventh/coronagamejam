﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Actors
{
    [RequireComponent(typeof(IMoveBehaviour))]
    [RequireComponent(typeof(IRotationBehaviour))]
    public class ZigzagEnemyActor : MonoBehaviour, IEnemyActor
    {
        IRotationBehaviour rotationBehaviour;
        IMoveBehaviour moveBehaviour;
        IDamageable damageable;
        IDamaging damagingBehaviour;

        GameplayManager manager;

        [SerializeField] Vector3 targetPos;

        float sizeBound;

        void Awake()
        {
            rotationBehaviour = GetComponent<IRotationBehaviour>();
            moveBehaviour = GetComponent<IMoveBehaviour>();
            damageable = GetComponent<IDamageable>();
            damagingBehaviour = GetComponent<IDamaging>();

            GameObject _gameplayManagerObject = GameObject.Find("GameplayManager");
            if (_gameplayManagerObject != null)
            {
                manager = _gameplayManagerObject.GetComponent<GameplayManager>();
                sizeBound = manager.camSize;
            }

            damageable.OnDamagedE += SlowDown;
            damageable.OnAfterDamagedE += NormalizeSpeed;
            damagingBehaviour.Init((int)Factions.player);

            SetNextZigzagTargetPosition();
            rotationBehaviour.Rotate(new Vector3(transform.position.x,-999,0));
        }

        void Update()
        {
            if (Vector2.Distance(transform.position, targetPos) < 0.1f)
                SetNextZigzagTargetPosition();
        }

        Vector3 randomZigzagContainer;
        void SetNextZigzagTargetPosition()
        {
            randomZigzagContainer.x = UnityEngine.Random.Range(-sizeBound, sizeBound);
            if (manager != null)
                randomZigzagContainer.y = transform.position.y - UnityEngine.Random.Range(1, manager.GetMainCam().orthographicSize / 2f);
            else
                randomZigzagContainer.y = transform.position.y - Random.Range(2, 10);
            randomZigzagContainer.z = 0;

            targetPos = randomZigzagContainer;
            moveBehaviour.Move(targetPos);
        }

        float NormalizeIfXOutOfBound(float _randomedX)
        {
            if (_randomedX < -manager.camSize)
                return -(manager.camSize - 1);
            else if (_randomedX > manager.camSize)
                return (manager.camSize - 1);
            else
                return _randomedX;
        }

        void SlowDown()
        {
            moveBehaviour.SetSpeedMultiplier(1);
        }

        void NormalizeSpeed()
        {
            moveBehaviour.SetSpeedMultiplier(1);
        }

        public void Die()
        {
            damageable.Die();
        }
    }
}