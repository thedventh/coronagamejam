﻿using UnityEngine;

namespace Actors
{
    [RequireComponent(typeof(IMoveBehaviour))]
    [RequireComponent(typeof(IRotationBehaviour))]
    public class SniperEnemyActor : MonoBehaviour, IEnemyActor
    {

        [SerializeField] AnimationEventExecutor animationEventExecutor;

        IMoveBehaviour moveBehaviour;
        IRotationBehaviour rotationBehaviour;
        IDamageable damageable;
        IAttackingBehaviour attackingBehaviour;
        

        GameplayManager manager;
        GameObject player;

        Vector3 targetMovePos;

        void Awake()
        {
            rotationBehaviour = GetComponent<IRotationBehaviour>();
            moveBehaviour = GetComponent<IMoveBehaviour>();
            damageable = GetComponent<IDamageable>();
            attackingBehaviour = GetComponent<IAttackingBehaviour>();

            player = GameObject.FindGameObjectWithTag(PublicParams.playerTag);
            manager = GameObject.Find("GameplayManager").GetComponent<GameplayManager>();

            animationEventExecutor.AnimationEvent = Fire;

            targetMovePos = new Vector3(transform.position.x, manager.GetMainCam().orthographicSize - 3, 0);
            moveBehaviour.Move(targetMovePos);
        }

        void Update()
        {
            FaceAtPlayer();
        }

        void FaceAtPlayer()
        {
            if(player != null)
                rotationBehaviour.Rotate(player.transform.position);
        }

        void Fire()
        {
            attackingBehaviour.Fire();
        }

        public void Die()
        {
            damageable.Die();
        }
    }
}