﻿using UnityEngine;

namespace Actors
{
    [RequireComponent(typeof(IMoveBehaviour))]
    [RequireComponent(typeof(IRotationBehaviour))]
    public class KamikazeEnemyActor : MonoBehaviour, IEnemyActor
    {
        IRotationBehaviour rotationBehaviour;
        IMoveBehaviour moveBehaviour;
        IDamageable damageable;
        IDamaging damagingBehaviour;

        GameObject player;

        bool ramToPlayer;

        void Awake()
        {
            rotationBehaviour = GetComponent<IRotationBehaviour>();
            moveBehaviour = GetComponent<IMoveBehaviour>();
            damageable = GetComponent<IDamageable>();
            damagingBehaviour = GetComponent<IDamaging>();

            player = GameObject.FindGameObjectWithTag(PublicParams.playerTag);

            damageable.OnDamagedE += SlowDown;
            damageable.OnAfterDamagedE += NormalizeSpeed;
            damagingBehaviour.Init((int)Factions.player);

            ramToPlayer = Random.Range(0, 100) > 85;
            if (!ramToPlayer)
                RamHeadquater();
        }

        void Update()
        {
            if(ramToPlayer)
                MoveToPlayer();
        }

        void RamHeadquater()
        {
            Vector3 _target = new Vector3(transform.position.x, -999, 0);
            moveBehaviour.Move(_target);
            rotationBehaviour.Rotate(_target);
        }

        void MoveToPlayer()
        {
            Vector3 _playerPos = Vector3.zero;
            if(player != null)
                _playerPos = player.transform.position;
            else
			{
				ramToPlayer = false;
				RamHeadquater();
			}

            rotationBehaviour.Rotate(_playerPos);
            moveBehaviour.Move(_playerPos);
        }

        void SlowDown()
        {
            moveBehaviour.SetSpeedMultiplier(-0.25f);
        }

        void NormalizeSpeed()
        {
            moveBehaviour.SetSpeedMultiplier(1);
        }

        public void Die()
        {
            damageable.Die();
        }
    }
}