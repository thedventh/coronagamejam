﻿using UnityEngine;

namespace Actors
{
    [RequireComponent(typeof(IMoveBehaviour))]
    public class PlayerActor : MonoBehaviour
    {
        IMoveBehaviour moveBehaviour;
        IAttackingBehaviour attackingBehaviour;
        IDamageable damageableBehaviour;

        GameplayManager manager;

        bool onDamaged;

        private void Awake()
        {
            moveBehaviour = GetComponent<IMoveBehaviour>();
            attackingBehaviour = GetComponent<IAttackingBehaviour>();
            damageableBehaviour = GetComponent<IDamageable>();
            manager = GameObject.Find("GameplayManager").GetComponent<GameplayManager>();

            damageableBehaviour.OnDamagedE += OnDamagedOn;
            damageableBehaviour.OnAfterDamagedE += OnDamagedOff;
        }

        void Update()
        {
            MoveToFinggerPosition();
        }

        void OnDamagedOn()
        {
            onDamaged = true;
            UIEvents.OnPlayerDamaged();
        }

        void OnDamagedOff()
        {
            onDamaged = false;
        }

        void MoveToFinggerPosition()
        {
            if (IsOutOfBounds(Input.mousePosition))
                return;

            if (onDamaged)
                return;

            moveBehaviour.Move(currenInputInWordPos);
        }

        Vector2 currenInputInWordPos;
        bool IsOutOfBounds(Vector2 _input)
        {
            currenInputInWordPos = manager.GetMainCam().ScreenToWorldPoint(_input);
            currenInputInWordPos.y += 2.25f;
            return (currenInputInWordPos.x > manager.camSize || currenInputInWordPos.x < -manager.camSize || currenInputInWordPos.y > manager.GetMainCam().orthographicSize || currenInputInWordPos.y < -manager.GetMainCam().orthographicSize);
        }
    }
}