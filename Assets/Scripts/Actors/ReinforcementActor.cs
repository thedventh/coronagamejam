﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;
using System;

namespace Actors
{
    [RequireComponent(typeof(IMoveBehaviour))]
    [RequireComponent(typeof(IRotationBehaviour))]
    public class ReinforcementActor : MonoBehaviour
    {
        [SerializeField] Text dialogueText;
        [SerializeField] Animator dialogAnimator;

        IRotationBehaviour rotationBehaviour;
        IMoveBehaviour moveBehaviour;
        IDamageable damageable;
        IAttackingBehaviour attackingBehaviour;

        GameplayManager manager;

        Vector3 targetPos;

        Vector3 dialogueBoxLocalPos;

        void Awake()
        {
            rotationBehaviour = GetComponent<IRotationBehaviour>();
            moveBehaviour = GetComponent<IMoveBehaviour>();
            damageable = GetComponent<IDamageable>();
            attackingBehaviour = GetComponent<IAttackingBehaviour>();

            manager = GameObject.Find("GameplayManager").GetComponent<GameplayManager>();

            damageable.OnDiedE += TellSorry;
            damageable.OnDiedE += manager.DecreaseReinforcement;

            dialogueBoxLocalPos = dialogAnimator.transform.localPosition;

            CalculateStandbyPosition();

            StartCoroutine(CheckIfArrivedAtPos());
        }

        IEnumerator CheckIfArrivedAtPos()
        {
            while(Vector2.Distance(transform.position,targetPos) > 0.1f)
            {
                yield return new WaitForEndOfFrame();
            }

            dialogAnimator.SetTrigger("close");
        }

        void CalculateStandbyPosition()
        {
            targetPos.x = transform.position.x;
            targetPos.y = -(manager.GetMainCam().orthographicSize - 4);
            targetPos.z = 0;

            moveBehaviour.Move(targetPos);
            rotationBehaviour.Rotate(targetPos);
        }

        void TellSorry()
        {
            int _rand = UnityEngine.Random.Range(0, 100);
            if (_rand < 35)
                dialogueText.text = "Sorry...";
            else if (_rand < 65)
                dialogueText.text = "ARRGHHH!!";
            else
                dialogueText.text = "NOOO!!!";

            dialogAnimator.transform.SetParent(null);
            dialogAnimator.transform.position = transform.position + dialogueBoxLocalPos;
            dialogAnimator.SetBool("die",true);

        }

        public void Die()
        {
            damageable.Die();
        }
    }
}