﻿using System;

public static class UIEvents 
{
    public static event Action OnGameOverE;
    public static event Action OnGameClearedE;

    public static event Action OnPlayerDamagedE;
    public static event Action OnAfterPlayerDamagedE;
    public static event Action<int> OnPlayerHPChangeE;

    public static event Action<int,int> OnEnemySpawnedE;

    public static void OnEnemySpawned(int _curren,int _max)
    {
        OnEnemySpawnedE?.Invoke(_curren,_max);
    }

    public static void OnGameOver()
    {
        OnGameOverE?.Invoke();
    }

    public static void OnGameCleared()
    {
        OnGameClearedE?.Invoke();
    }

    public static void OnPlayerDamaged()
    {
        OnPlayerDamagedE?.Invoke();
    }

    public static void OnAfterPlayerDamaged()
    {
        OnAfterPlayerDamagedE?.Invoke();
    }

    public static void OnPlayerHPChange(int _currenHealth)
    {
        OnPlayerHPChangeE?.Invoke(_currenHealth);
    }
}
