﻿using System;

public static class GlobalGameplayEvents
{
    public static int selectedStageLevel;
    public static int totalKills;

    public static event Action OnEnemyCrossTheLineE;
    public static event Action OnEnemyDeadE;

    public static void SetTotalKills(int _totalKills)
    {
        totalKills = _totalKills;
    }

    public static void SetStageLevel(int _stageLevel)
    {
        selectedStageLevel = _stageLevel;
    }

    public static void OnEnemyCrossTHeLine()
    {
        OnEnemyCrossTheLineE?.Invoke();
    }

    public static void OnEnemyDead()
    {
        OnEnemyDeadE?.Invoke();
    }
}
