﻿using System.Collections.Generic;
using System.Collections;
using System.Linq;
using UnityEngine;
using System;

public class GameplayManager : MonoBehaviour
{
    [SerializeField] Transform spawnTransform;
    [SerializeField] List<LevelData> levelDataList;
    [SerializeField] Camera mainCam;
    [SerializeField] Animator damagedCanvasAnimation;
    [SerializeField] Transform endLineTransform;
    [SerializeField] GameObject reinforcementPrefab;

    IEnumerator shakeCamCoroutine;
    LevelData currenLevelData;

    [HideInInspector] public float camSize;
    float timerOnRun;
    int enemySpawnedCount;

    int enemyKilled;
    int reinforcementCount;
    int maxReinforcementCount = 3;
    bool isGameover;

    Actors.PlayerActor player;

    private void OnEnable()
    {
        GlobalGameplayEvents.OnEnemyDeadE += OnOneEnemyDown;
        UIEvents.OnGameOverE += IsGameover;
    }

    private void OnDisable()
    {
        GlobalGameplayEvents.OnEnemyDeadE -= OnOneEnemyDown;
        UIEvents.OnGameOverE -= IsGameover;
    }

    void Awake()
    {
        camSize = (mainCam.orthographicSize - 1) * Screen.width / Screen.height;
        SelectLevel(GlobalGameplayEvents.selectedStageLevel);
    }

    public void SetPlayer(Actors.PlayerActor _player)
    {
        player = _player;
    }

    void IsGameover()
    {
        isGameover = true;
    }

    void SelectLevel(int _level)
    {
        isGameover = false;
        reinforcementCount = 0;
        enemyKilled = 0;
        GlobalGameplayEvents.SetTotalKills(enemyKilled);

        StopAllCoroutines();

        currenLevelData = levelDataList.First(x => x.level == _level);

        UIEvents.OnEnemySpawned(0, currenLevelData.enemyAmount);

        StartCoroutine(SpawnTimerCoroutine());
    }

    IEnumerator SpawnTimerCoroutine()
    {
        timerOnRun = 0;
        while (enemySpawnedCount < currenLevelData.enemyAmount)
        {
            timerOnRun -= Time.deltaTime;

            if(timerOnRun <= 0)
            {
                timerOnRun = currenLevelData.enemySpawnRate;
                SpawnRandomEnemy();
            }

            yield return new WaitForEndOfFrame();
        }

        if(!isGameover)
            StartCoroutine(CheckIfThereIsNoEnemy(UIEvents.OnGameCleared));
    }

    void SpawnRandomEnemy()
    {
        int _randomSpawnNUmber = UnityEngine.Random.Range(0, currenLevelData.maxSpawnNumber);

        for (int i = 0; i < _randomSpawnNUmber; i++)
        {
            if (enemySpawnedCount >= currenLevelData.enemyAmount)
                return;

            int _randomedEnemy = UnityEngine.Random.Range(0, currenLevelData.enemyList.Count);
            int _randomSpawnPoint = UnityEngine.Random.Range((int)(-1*camSize), (int)(camSize));
            Vector3 _spawnPosition = new Vector3(_randomSpawnPoint, spawnTransform.position.y, 0);

            Instantiate(currenLevelData.enemyList[_randomedEnemy], _spawnPosition, spawnTransform.rotation);

            enemySpawnedCount++;

            UIEvents.OnEnemySpawned(enemySpawnedCount, currenLevelData.enemyAmount);

        }
    }

    IEnumerator CheckIfThereIsNoEnemy(Action _Callback = null)
    {
        while(GameObject.FindGameObjectsWithTag(PublicParams.enemyTag).Length > 0)
        {
            yield return new WaitForSeconds(0.5f);
        }

        _Callback?.Invoke();
    }

    void OnOneEnemyDown()
    {
        if (isGameover)
            return;

        enemyKilled++;
        GlobalGameplayEvents.SetTotalKills(enemyKilled);

        if (enemyKilled % 10 == 0)
            PowerUp();
    }

    void PowerUp()
    {
        SpawnReinforcement();
    }

    void SpawnReinforcement()
    {
        if (reinforcementCount >= maxReinforcementCount)
            return;

        int _randomSpawnPoint = UnityEngine.Random.Range((int)(-1 * camSize), (int)(camSize));
        Instantiate(reinforcementPrefab, new Vector3(_randomSpawnPoint, endLineTransform.position.y, 0), endLineTransform.rotation);
        reinforcementCount++;
    }

    public void DecreaseReinforcement()
    {
        reinforcementCount--;
    }

    public Camera GetMainCam()
    {
        return mainCam;
    }

    [System.Serializable]
    public class LevelData
    {
        public int level;
        public float enemySpawnRate;
        public int enemyAmount = 5;
        public int maxSpawnNumber = 3;
        public List<GameObject> enemyList;
    }
}
