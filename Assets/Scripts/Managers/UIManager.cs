﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public enum SceneName
{
	StartScene,
	GamePlayScene,
	EmptyScene
}

public class UIManager : UIMenu 
{
    public MenuSettings menuSettingsData;
	public bool changeScenes;											//If true, load a new scene when Start is pressed, if false, fade out UI and continue in single scene
	public bool changeMusicOnStart;										//Choose whether to continue playing menu music or start a new music clip
    public CanvasGroup fadeOutImageCanvasGroup;                         //Canvas group used to fade alpha of image which fades in before changing scenes
    public Image fadeImage;                                             //Reference to image used to fade out before changing scenes

	[HideInInspector] public bool inMainMenu = true;					//If true, pause button disabled in main menu (Cancel in input manager, default escape key)
	[HideInInspector] public AnimationClip fadeAlphaAnimationClip;		//Animation clip fading out UI elements alpha


	private PlayMusic playMusic;										//Reference to PlayMusic script
	private Pause pause;
	private float fastFadeIn = .01f;									//Very short fade time (10 milliseconds) to start playing music immediately without a click/glitch
	private ShowPanels showPanels;										//Reference to ShowPanels script on UI GameObject, to show and hide panels
    private CanvasGroup menuCanvasGroup;
	[SerializeField] MenuGameOver panelGameOver;

    void Awake()
	{
		//Get a reference to ShowPanels attached to UI object
		showPanels = GetComponent<ShowPanels> ();

		//Get a reference to PlayMusic attached to UI object
		playMusic = GetComponent<PlayMusic> ();
		pause = GetComponent<Pause> ();

        //Get a reference to the CanvasGroup attached to the main menu so that we can fade it's alpha
        menuCanvasGroup = GetComponent<CanvasGroup>();

        fadeImage.color = menuSettingsData.sceneChangeFadeColor;
	}

	void OnEnable()
    {
        SceneManager.sceneLoaded += SceneWasLoaded;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= SceneWasLoaded;
    }

	void OnGameOver ()
    {
		showPanels.panelTint.SetActive(true);
        panelGameOver.Show();
    }

	public void StartGame()
	{
		//If changeMusicOnStart is true, fade out volume of music group of AudioMixer by calling FadeDown function of PlayMusic
		//To change fade time, change length of animation "FadeToColor"
		if (menuSettingsData.musicGame != null) 
		{
			playMusic.FadeDown(menuSettingsData.menuFadeTime);
		}

		menuSettingsData.nextSceneIndex = (int)SceneName.GamePlayScene;
		StartCoroutine(FadeCanvasGroupAlpha(0f, 1f, fadeOutImageCanvasGroup, OnGameStartLoading));
	}

    //Once the level has loaded, check if we want to call PlayLevelMusic
    void SceneWasLoaded(Scene scene, LoadSceneMode mode)
    {
		//if changeMusicOnStart is true, call the PlayLevelMusic function of playMusic
		if (menuSettingsData.musicGame != null)
		{
			playMusic.PlayLevelMusic ();
		}	
	}

	public void StartGameInScene()
	{
		//Pause button now works if escape is pressed since we are no longer in Main menu.
		inMainMenu = false;

		//If there is a second music clip in MenuSettings, fade out volume of music group of AudioMixer by calling FadeDown function of PlayMusic 
		if (menuSettingsData.musicGame != null) 
		{
			//Wait until game has started, then play new music
			Invoke ("PlayNewMusic", menuSettingsData.menuFadeTime);
		}
        
        StartCoroutine(FadeCanvasGroupAlpha(0f,1f, fadeOutImageCanvasGroup, OnGameStartLoading));
	}

	private void OnGameStartLoading ()
	{
		//Pause button now works if escape is pressed since we are no longer in Main menu.
		inMainMenu = false;

		showPanels.HideMenu ();
		showPanels.ShowGamePlay();
		
		panelGameOver.Hide();
		
		//Load the selected scene, by scene index number in build settings
		SceneManager.LoadScene (menuSettingsData.nextSceneIndex);

		StartCoroutine(FadeCanvasGroupAlpha(1f,0f, fadeOutImageCanvasGroup, OnGameFinishLoading));
	}

	private void OnGameFinishLoading ()
	{
		UIEvents.OnGameOverE += OnGameOver;
	}

	private void OnGameStartExit()
	{
		showPanels.HidePausePanel();
		showPanels.ShowMenu();
		showPanels.HideGamePlay();

		panelGameOver.Hide();

		StartCoroutine(FadeCanvasGroupAlpha(1f,0f, fadeOutImageCanvasGroup));
	}

    public void PlayNewMusic()
	{
		//Fade up music nearly instantly without a click 
		playMusic.FadeUp (fastFadeIn);
		//Play second music clip from MenuSettings
		playMusic.PlaySelectedMusic (menuSettingsData.musicGame);
	}

	public void ExitGame ()
	{
		UIEvents.OnGameOverE -= OnGameOver;

		pause.UnPause();
		menuSettingsData.nextSceneIndex = (int)SceneName.EmptyScene;
		SceneManager.LoadScene (menuSettingsData.nextSceneIndex);
		StartCoroutine(FadeCanvasGroupAlpha(0f,1f, fadeOutImageCanvasGroup, OnGameStartExit));
	}	
}
