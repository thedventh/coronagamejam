using System;
using UnityEngine;
using UnityEngine.UI;

public class MenuGameOver : UIMenu
{
    [SerializeField] Text textMessage;
    [SerializeField] Text textVirus;
    [SerializeField] Text textVirusCorona;
    [SerializeField] Pause pause;
    [SerializeField] UIManager uIManager;
    
    public void Show ()
    {
        gameObject.SetActive(true);
        textVirus.text = GlobalGameplayEvents.totalKills.ToString();
        
        StartCoroutine(FadeCanvasGroupAlpha(0.0f, 1.0f, canvasGroup));
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    
}