using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIMenu : MonoBehaviour
{
    public CanvasGroup canvasGroup;

    internal IEnumerator FadeCanvasGroupAlpha(float startAlpha, float endAlpha, CanvasGroup canvasGroupToFadeAlpha, Action OnCompleted = null)
    {
		canvasGroupToFadeAlpha.gameObject.SetActive(true);

        float elapsedTime = 0f;
        float totalDuration = 1;

        while (elapsedTime < totalDuration)
        {
            elapsedTime += Time.deltaTime;
            float currentAlpha = Mathf.Lerp(startAlpha, endAlpha, elapsedTime / totalDuration);
            canvasGroupToFadeAlpha.alpha = currentAlpha;
            yield return null;
        }

		canvasGroupToFadeAlpha.gameObject.SetActive(endAlpha>0);

        OnCompleted?.Invoke();
    }
}