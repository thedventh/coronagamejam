﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class ShowPanels : MonoBehaviour {

	public GameObject panelOption;							//Store a reference to the Game Object OptionsPanel 
	public GameObject panelTint;							//Store a reference to the Game Object OptionsTint 
	public GameObject panelMenu;							//Store a reference to the Game Object MenuPanel 
	public GameObject panelPause;                           //Store a reference to the Game Object PausePanel 
	public GameObject panelGameplay;

    private GameObject activePanel;                         
    private MenuObject activePanelMenuObject;
    private EventSystem eventSystem;



    private void SetSelection(GameObject panelToSetSelected)
    {
        activePanel = panelToSetSelected;
        activePanelMenuObject = activePanel.GetComponent<MenuObject>();
        if (activePanelMenuObject != null)
        {
            activePanelMenuObject.SetFirstSelected();
        }
    }

    public void Start()
    {
        SetSelection(panelMenu);
    }

    //Call this function to activate and display the Options panel during the main menu
    public void ShowOptionsPanel()
	{
		panelOption.SetActive(true);
		panelTint.SetActive(true);
        panelMenu.SetActive(false);
        SetSelection(panelOption);

    }

	//Call this function to deactivate and hide the Options panel during the main menu
	public void HideOptionsPanel()
	{
        panelMenu.SetActive(true);
        panelOption.SetActive(false);
		panelTint.SetActive(false);
	}

	//Call this function to activate and display the main menu panel during the main menu
	public void ShowMenu()
	{
		panelMenu.SetActive (true);
        SetSelection(panelMenu);
    }

	//Call this function to deactivate and hide the main menu panel during the main menu
	public void HideMenu()
	{
		panelMenu.SetActive (false);

	}
	
	//Call this function to activate and display the Pause panel during game play
	public void ShowPausePanel()
	{
		panelPause.SetActive (true);
		panelTint.SetActive(true);
        SetSelection(panelPause);
    }

	//Call this function to deactivate and hide the Pause panel during game play
	public void HidePausePanel()
	{
		panelPause.SetActive (false);
		panelTint.SetActive(false);

	}

	public void ShowGamePlay()
	{
		HideMenu();
		panelGameplay.SetActive(true);
	}

	public void HideGamePlay()
	{
		panelGameplay.SetActive(false);
		ShowMenu();
	}

	public void ExitGamePlay()
	{
		panelGameplay.SetActive(false);
		
	}
}
