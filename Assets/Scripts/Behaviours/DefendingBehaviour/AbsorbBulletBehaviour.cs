﻿using UnityEngine;

namespace Behaviours
{
    [RequireComponent(typeof(Collider2D))]
    public class AbsorbBulletBehaviour : MonoBehaviour
    {
        [SerializeField] Factions toAbsorb;
        [SerializeField] GameObject VFX;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if(collision.gameObject != null)
            {
                switch (toAbsorb)
                {
                    case Factions.enemy:
                        if (collision.gameObject.tag == PublicParams.enemyBulletTag)
                        {
                            InstantiateVFX(collision.gameObject.transform.position);
                            Destroy(collision.gameObject);
                        }
                        break;
                    case Factions.player:
                        if (collision.gameObject.tag == PublicParams.playerBulletTag)
                        {
                            InstantiateVFX(collision.gameObject.transform.position);
                            Destroy(collision.gameObject);
                        }
                        break;
                    default:
                        if (collision.gameObject.tag == PublicParams.bulletTag)
                        {
                            InstantiateVFX(collision.gameObject.transform.position);
                            Destroy(collision.gameObject);
                        }
                        break;
                }
            }
        }

        void InstantiateVFX(Vector2 _contactPoint)
        {
            if (VFX == null)
                return;

            Instantiate(VFX, new Vector3(_contactPoint.x, _contactPoint.y, 0), transform.rotation);
        }
    }
}