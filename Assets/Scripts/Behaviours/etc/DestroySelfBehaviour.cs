﻿using System.Collections;
using UnityEngine;

namespace Behaviours
{
    public class DestroySelfBehaviour : MonoBehaviour
    {
        [SerializeField] float timer;
        float timerOnRun;

        private void OnEnable()
        {
            StartCoroutine(DestroySelfTimer());
        }

        private void OnDisable()
        {
            StopAllCoroutines();
        }

        IEnumerator DestroySelfTimer()
        {
            timerOnRun = timer;

            while(timerOnRun> 0 )
            {
                timerOnRun -= Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }

            Destroy(gameObject);
        }
    }
}