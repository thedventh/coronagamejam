﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Behaviours
{
    public class DestroySelfIfOutOfBoundsBehaviour : MonoBehaviour
    {
        GameplayManager manager;
        Vector2 bound;

        private void Awake()
        {
            manager = GameObject.Find("GameplayManager").GetComponent<GameplayManager>();
            float _xBound = (manager.GetMainCam().orthographicSize) * Screen.width / Screen.height;
            bound = new Vector2(_xBound, manager.GetMainCam().orthographicSize);
        }

        private void Update()
        {
            if (transform.position.x > (bound.x) || transform.position.x < -(bound.x) || transform.position.y > (bound.y) || transform.position.y < -(bound.y))
                Destroy(gameObject);
        }
    }
}