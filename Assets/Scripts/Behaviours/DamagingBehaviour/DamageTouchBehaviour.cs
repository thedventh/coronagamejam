﻿using UnityEngine;

namespace Behaviours
{
    [RequireComponent(typeof(Collider2D))]
    public class DamageTouchBehaviour : MonoBehaviour, IDamaging
    {
        [SerializeField] int damage;

        int faction;

        public void Init(int _targetFaction)
        {
            faction = _targetFaction;
        }

        string mTagToCheck;
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (PublicParams.IsEnemyFaction(faction))
                mTagToCheck = PublicParams.enemyTag;
            else if (PublicParams.IsPlayerFaction(faction))
                mTagToCheck = PublicParams.playerTag;
            else
                mTagToCheck = "";

            if (!string.IsNullOrEmpty(mTagToCheck))
            {
                if (collision.gameObject.tag == mTagToCheck)
                {
                    collision.GetComponent<IDamageable>().TakeDamage(damage);
                }
            }else
            {
                if(collision.gameObject != null)
                {
                    IDamageable _damageable = collision.GetComponent<IDamageable>();
                    if (_damageable != null)
                        _damageable.TakeDamage(damage);
                }
            }
        }
    }
}