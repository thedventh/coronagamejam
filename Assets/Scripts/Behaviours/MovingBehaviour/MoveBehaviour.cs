﻿using UnityEngine;

namespace Behaviours
{
    public class MoveBehaviour : MonoBehaviour, IMoveBehaviour
    {
        [SerializeField] float moveSpeed = 1;

        float initialMoveSpeed;
        Vector3 targetPos;

        void Awake()
        {
            initialMoveSpeed = moveSpeed;
        }

        void Update()
        {
            transform.position = Vector2.MoveTowards(transform.position, targetPos, moveSpeed * Time.deltaTime);
        }

        public void Move(Vector3 _targetPos)
        {
            targetPos = _targetPos;
        }

        public void SetSpeedMultiplier(float _multiplier)
        {
            moveSpeed = _multiplier * initialMoveSpeed;
        }
    }
}