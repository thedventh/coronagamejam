﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Behaviours
{
    public class MoveForwardOnlyBehaviour : MonoBehaviour, IMoveBehaviour
    {
        [SerializeField] float speed;

        float initialSpeed;
        float currenSpeed;

        void Awake()
        {
            initialSpeed = speed;
            currenSpeed = speed;
        }

        void Update()
        {
            transform.Translate(Vector2.up * currenSpeed * Time.deltaTime);
        }

        public void Move(Vector3 _targetPos)
        {
            
        }

        public void SetSpeedMultiplier(float _multiplier)
        {
            currenSpeed = initialSpeed *  _multiplier;
        }
    }
}