﻿using System;
using UnityEngine;

namespace Behaviours
{
    [RequireComponent(typeof(Collider2D))]
    public class PlayerDamageableBehaviour : MonoBehaviour, IDamageable
    {
        [SerializeField] int maxHealth = 10;
        [SerializeField] GameObject VFX;

        public event System.Action OnDamagedE;
        public event System.Action OnAfterDamagedE;
        public event Action OnDiedE;

        [SerializeField] int currenHealth;
        bool onInvicieble;

        GameplayManager manager;


        void OnEnable()
        {
            UIEvents.OnAfterPlayerDamagedE += AfterTakeDamage;
            GlobalGameplayEvents.OnEnemyCrossTheLineE += TakeOneDamage;
        }

        void OnDisable()
        {
            UIEvents.OnAfterPlayerDamagedE -= AfterTakeDamage;
            GlobalGameplayEvents.OnEnemyCrossTheLineE -= TakeOneDamage;
        }

        void Awake()
        {
            manager = GameObject.Find("GameplayManager").GetComponent<GameplayManager>();

            currenHealth = maxHealth;
        }

        public void TakeDamage(int _damage)
        {
            if (onInvicieble)
                return;

            onInvicieble = true;

            currenHealth -= _damage;

            UIEvents.OnPlayerHPChange(currenHealth);
            OnDamagedE?.Invoke();

            if (currenHealth <= 0)
                Die();
        }

        void AfterTakeDamage()
        {
            onInvicieble = false;
            OnAfterDamagedE?.Invoke();
        }

        void TakeOneDamage()
        {
            TakeDamage(1);
        }

        public void Die()
        {
            if (VFX != null)
                Instantiate(VFX, transform.position, transform.rotation);

            Destroy(gameObject);

            Debug.Log("gameover");
            UIEvents.OnGameOver();
        }
    }

}