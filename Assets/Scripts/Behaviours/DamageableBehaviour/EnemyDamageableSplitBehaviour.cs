﻿using UnityEngine;

namespace Behaviours
{
    public class EnemyDamageableSplitBehaviour : EnemyDamageableBehaviour
    {
        [SerializeField] GameObject splatFX;

        [HideInInspector] public bool isHalfed;
        [HideInInspector] public bool isQuartered;

        public override void TakeDamage(int _damage)
        {
            base.TakeDamage(_damage);

            if(currenHealth <= (int)((float)maxHealth *0.25f) && !isQuartered)
            {
                isQuartered = true;
                Instantiate(splatFX, transform.position, transform.rotation);
                float _xPoss = transform.position.x;

                float _toAPply = IsOutOfBound(_xPoss + transform.localScale.x) ? _xPoss : _xPoss + transform.localScale.x;
                transform.position = new Vector3(_toAPply, transform.position.y, 0);

                _toAPply = IsOutOfBound(_xPoss - transform.localScale.x) ? _xPoss : _xPoss - transform.localScale.x;
                Instantiate(gameObject, new Vector3(_toAPply, transform.position.y, 0), transform.rotation);
            }
            else if(currenHealth <= (int)((float)maxHealth *0.5f)&&!isHalfed)
            {
                isHalfed = true;
                Instantiate(splatFX, transform.position, transform.rotation);
                float _xPoss = transform.position.x;

                float _toAPply = IsOutOfBound(_xPoss + transform.localScale.x) ? _xPoss : _xPoss + transform.localScale.x;
                transform.position = new Vector3(_toAPply, transform.position.y, 0);

                _toAPply = IsOutOfBound(_xPoss - transform.localScale.x) ? _xPoss : _xPoss - transform.localScale.x;
                Instantiate(gameObject, new Vector3(_toAPply, transform.position.y, 0), transform.rotation);
            }
        }

        bool IsOutOfBound(float _xPos)
        {
            return (_xPos > manager.camSize -1 || _xPos < -manager.camSize + 1);
        }
    }
}