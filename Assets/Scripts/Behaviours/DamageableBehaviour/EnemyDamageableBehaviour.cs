﻿using System;
using System.Collections;
using UnityEngine;

namespace Behaviours
{
    public class EnemyDamageableBehaviour : MonoBehaviour, IDamageable
    {
        [SerializeField] public int maxHealth = 3;
        [SerializeField] Material damagedMaterial;
        [SerializeField] SpriteRenderer spriteRenderer;

        [SerializeField] GameObject diedSplatFX;

        public event System.Action OnDamagedE;
        public event System.Action OnAfterDamagedE;

        public event Action OnDiedE;

        [HideInInspector] public int currenHealth;

        [HideInInspector] public Material initialMaterial;
        Vector3 initialScale;
        float scalingOffset = 0.1f;

        Vector3 currenTargetScale;

        [HideInInspector] public bool isInitted;
        bool onInvicieble;

        protected GameplayManager manager;

        void Awake()
        {
            GameObject _gameplayManagerObject = GameObject.Find("GameplayManager");
            if(_gameplayManagerObject != null)
                manager = _gameplayManagerObject.GetComponent<GameplayManager>();

            if(initialMaterial == null)
                initialMaterial = spriteRenderer.material;

            initialScale = transform.localScale;

            if(!isInitted)
                currenHealth = maxHealth;

            isInitted = true;

            onInvicieble = true;

            Invoke("StopInviecieble", 0.25f);

            CalculateTargetScale();
            transform.localScale = currenTargetScale;

            ReturnBackMaterial();
        }

        public virtual void TakeDamage(int _damage)
        {
            if (onInvicieble)
                return;

            onInvicieble = true;

            currenHealth -= _damage;

            ScaleBasedOnHealth();
        }

        public void Die()
        {
            Instantiate(diedSplatFX, transform.position, transform.rotation);
            Destroy(gameObject);
            GlobalGameplayEvents.OnEnemyDead();
        }

        void ScaleBasedOnHealth()
        {
            CalculateTargetScale();

            StopAllCoroutines();
            StartCoroutine(DamagedCoroutine());
        }

        void CalculateTargetScale()
        {
            float _targetScale = initialScale.x - PublicParams.minimalScale;
            _targetScale *= (float)currenHealth / (float)maxHealth;
            _targetScale += PublicParams.minimalScale;
            if (_targetScale < PublicParams.minimalScale)
                _targetScale = PublicParams.minimalScale;

            currenTargetScale = new Vector3(_targetScale, _targetScale, _targetScale);
        }

        void ReturnBackMaterial()
        {
            spriteRenderer.material = initialMaterial;
        }

        Vector3 tempScaleContainer;
        IEnumerator DamagedCoroutine()
        {
            OnDamagedE?.Invoke();

            spriteRenderer.material = damagedMaterial;
            while(transform.localScale.x > (currenTargetScale.x - scalingOffset))
            {
                tempScaleContainer = transform.localScale;

                tempScaleContainer.x -= 3 * Time.deltaTime;
                tempScaleContainer.y -= 3 * Time.deltaTime;
                tempScaleContainer.z -= 3 * Time.deltaTime;

                transform.localScale = tempScaleContainer;

                yield return new WaitForEndOfFrame();
            }

            ReturnBackMaterial();

            while (transform.localScale.x < currenTargetScale.x)
            {
                tempScaleContainer = transform.localScale;

                tempScaleContainer.x += Time.deltaTime;
                tempScaleContainer.y += Time.deltaTime;
                tempScaleContainer.z += Time.deltaTime;

                transform.localScale = tempScaleContainer;

                yield return new WaitForEndOfFrame();
            }

            OnAfterDamagedE?.Invoke();

            if (currenHealth <= 0)
                Die();
            else
                onInvicieble = false;
        }

        void StopInviecieble()
        {
            onInvicieble = false;
        }
    }
}
