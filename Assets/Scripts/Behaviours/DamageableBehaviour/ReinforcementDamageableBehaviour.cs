﻿using System;
using UnityEngine;

namespace Behaviours
{
    [RequireComponent(typeof(Collider2D))]
    public class ReinforcementDamageableBehaviour : MonoBehaviour, IDamageable
    {
        [SerializeField] int maxHealth = 10;
        [SerializeField] GameObject dieVFX;

        public event System.Action OnDamagedE;
        public event System.Action OnAfterDamagedE;
        public event Action OnDiedE;

        int currenHealth;

        GameplayManager manager;

        void Awake()
        {
            manager = GameObject.Find("GameplayManager").GetComponent<GameplayManager>();

            currenHealth = maxHealth;
        }

        public void TakeDamage(int _damage)
        {
            currenHealth -= _damage;

            if (currenHealth <= 0)
                Die();
        }

        void TakeOneDamage()
        {
            TakeDamage(1);
        }

        public void Die()
        {
            if (dieVFX != null)
                Instantiate(dieVFX, transform.position, transform.rotation);

            OnDiedE?.Invoke();

            Destroy(gameObject);
        }
    }
}