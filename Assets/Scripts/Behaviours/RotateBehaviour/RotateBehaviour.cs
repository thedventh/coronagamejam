﻿using UnityEngine;

namespace Behaviours
{
    public class RotateBehaviour : MonoBehaviour, IRotationBehaviour
    {
        //show in inspector variables
        [SerializeField] float rotateSpeed = 10;

        //private variables
        Vector2 faceTargetPos;

        public void Rotate(Vector3 _target)
        {
            FaceToPossition(_target);
        }

        void FaceToPossition(Vector3 _targetPos)
        {
            Vector3 _diff = _targetPos - transform.position;
            _diff.Normalize();

            float _zRotation = Mathf.Atan2(_diff.y, _diff.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0f, 0f, _zRotation - 90);
        }
    }
}