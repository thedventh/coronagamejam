﻿using System.Collections;
using UnityEngine;

namespace Behaviours
{
    [RequireComponent(typeof(AudioSource))]
    public class FiringBehaviour : MonoBehaviour, IAttackingBehaviour
    {
        [SerializeField] float fireRate = 0.5f;
        [SerializeField] Transform barrrelTip;
        [SerializeField] GameObject bulletPrefab;
        [SerializeField] Factions targetFaction;
        [SerializeField] AudioClip firingSFX;
        [SerializeField] bool autofire = true;

        int faction;
        AudioSource audioSource;
        float timerOnRun;

        private void OnEnable()
        {
            if(autofire)
                StartCoroutine(Firing());
        }

        private void OnDisable()
        {
            StopAllCoroutines();
        }

        void Awake()
        {
            audioSource = GetComponent<AudioSource>();

            if (gameObject.tag == PublicParams.enemyTag)
                faction = (int)Factions.enemy;
            else if (gameObject.tag == PublicParams.playerTag)
                faction = (int)Factions.player;
            else
                faction = 0;
        }

        IEnumerator Firing()
        {
            timerOnRun = 0;
            while(true)
            {
                timerOnRun += Time.deltaTime;

                if(timerOnRun >= fireRate)
                {
                    timerOnRun = 0;
                    Fire();
                }

                yield return new WaitForEndOfFrame();
            }
        }

        IDamaging mDamagingBehaviour;
        IFireBehaviour mFireBehaviour;
        GameObject bulletContainer;
        public void Fire()
        {
            bulletContainer = Instantiate(bulletPrefab, barrrelTip.position, transform.rotation);
            mDamagingBehaviour = bulletContainer.GetComponent<IDamaging>();
            mFireBehaviour = bulletContainer.GetComponent<IFireBehaviour>();

            if (mDamagingBehaviour != null)
                mDamagingBehaviour.Init((int)targetFaction);

            if (mFireBehaviour != null)
                mFireBehaviour.Init(faction);

            audioSource.PlayOneShot(firingSFX);
        }
    }
}