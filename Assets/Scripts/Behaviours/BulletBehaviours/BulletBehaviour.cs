﻿using UnityEngine;

namespace Behaviours
{
    public class BulletBehaviour : MonoBehaviour, IFireBehaviour
    {
        public void Init(int _faction)
        {
            if (PublicParams.IsEnemyFaction(_faction))
                gameObject.tag = PublicParams.enemyBulletTag;
            else if (PublicParams.IsPlayerFaction(_faction))
                gameObject.tag = PublicParams.playerBulletTag;
            else
                gameObject.tag = PublicParams.bulletTag;
        }
    }
}